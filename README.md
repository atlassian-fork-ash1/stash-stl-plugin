STL 3D Plugin (for Bitbucket Server)
=========================


A plugin for [Atlassian Bitbucket Server](https://www.atlassian.com/software/stash/overview) to render .stl files
as 3D models when viewing their source. Rendering the diffs as a 3D model has not been implemented yet.

Originally created by Xu-Heng and Yiyang as part of [Atlassian ShipIt](https://www.atlassian.com/company/about/shipit)
competition.

### Compatibility ###

Requires Atlassian Bitbucket 4.0 or higher

### Licensing ###

Released under Apache License, Version 2.0

Uses:

* three.js - MIT License


### Source ###

Hosted on [Atlassian Bitbucket](https://bitbucket.org/atlassian/stash-stl-plugin).

Clone from:

```
git@bitbucket.org:atlassian/stash-stl-plugin.git
```

### Development ###

This is an Atlassian plugin so you will need the
[Atlassian SDK installed](https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK).

Here are the SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
