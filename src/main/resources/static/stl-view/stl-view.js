define('stl/stl-view', [
    'jquery',
    'stl/stl-renderer'
], function(
    $,
    STLRenderer
) {

    /**
     * Constructs a view that renders a .stl file in 3D with a toolbar to toggle between different mesh materials
     *
     * @param $container - container to render the .stl file in
     * @param stlRawUrl - raw url to the .stl file
     * @constructor
     */
    function STLView($container, stlRawUrl) {
        this.$view = $(bitbucket.feature.fileContent.stl.view());
        this.$container = $container.html(this.$view);
        this.stlRenderer = new STLRenderer(this.$view);
        this.initToolbar();

        this.stlRenderer.load(stlRawUrl);
        this.stlRenderer.render();
    }

    STLView.prototype.initToolbar = function() {
        var $toolbar = $(bitbucket.feature.fileContent.stl.toolbar());
        this.$view.prepend($toolbar);

        var self = this;
        $toolbar.find('.aui-button').on('click', function() {
            var $this = $(this);
            if ($this.attr('aria-pressed') === 'true') {
                return false;
            }

            $toolbar.find('.aui-button[aria-pressed="true"]').removeAttr('aria-pressed');
            $this.attr('aria-pressed', true);

            var mode = $this.attr('data-mode');
            self.stlRenderer.setMeshMode(mode);
        });
    };

    /**
     * This will be called when the view is destroyed
     */
    STLView.prototype.destroy = function() {
        this.stlRenderer.destroy();
        this.stlRenderer = null;
    };

    return STLView;
});
