/**
 * Just an amd wrapper for three.js
 */
define('lib/threejs', function() {
    return THREE;
});