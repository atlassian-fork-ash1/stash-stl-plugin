define('stl/stl-renderer', [
    'jquery',
    'underscore',
    'lib/threejs'
], function(
    $,
    _,
    THREE
) {

    var meshColors = {
        ambient: 0x26588B,
        color: 0x3779BB,
        specular: 0x26588B,
        shininess: 100
    };
    var renderColor = 0xFFFFFF;
    var lightColor = 0xccd9ea;
    var ambientLightColor = 0x909090;

    var rotateDelay = 3000;
    var rotateIncrement = 0.005;

    var gridSteps = 0.1;
    var gridSize = 1.2;
    var gridLineColor = 0x707070;
    var yPlaneAdjustment = -0.2;

    var cameraFOV = 50;

    var defaultMode = 'surface';

    function STLRenderer($container) {
        this.$container = $container;
        this.rotate = false;
        this.width = this.$container.innerWidth();
        this.height = this._calculateHeight();

        this.renderer = this._initRenderer();
        this.$webGlElement = $(this.renderer.domElement);
        this.rootObject = new THREE.Object3D();
        this.camera = this._initCamera();
        this.scene = this._initScene();
        this.light = this._initLights();
        this.loader = new THREE.STLLoader();
        this.controls = this._initControls();

        this.$container.html(this.$webGlElement);

        _.bindAll(this, '_onWindowResize', 'render');
        $(window).on('resize', this._onWindowResize);
    }

    STLRenderer.modes = {
        SURFACE: 'surface',
        SOLID: 'solid',
        WIREFRAME: 'wireframe'
    };

    STLRenderer.prototype._initRenderer = function() {
        var renderer = new THREE.WebGLRenderer();
        renderer.setSize(this.width, this.height);
        renderer.setClearColor(renderColor, 1);
        renderer.gammaInput = true;
        renderer.gammaOutput = true;
        renderer.physicallyBasedShading = true;
        renderer.alpha = false;
        renderer.antialias = true;

        return renderer;
    };

    STLRenderer.prototype._initCamera = function() {
        var aspect = this._calculateAspectRatio();
        var camera = new THREE.PerspectiveCamera(cameraFOV, aspect, 0.1, 1000);
        camera.position.set(0, 0.75, 2.25);

        var cameraTarget = new THREE.Vector3(0, 0, 0);
        camera.lookAt(cameraTarget);
        return camera;
    };

    STLRenderer.prototype._initScene = function() {
        var scene = new THREE.Scene();
        scene.add(this.rootObject);

        var gridHelper = new THREE.GridHelper(gridSize, gridSteps);
        gridHelper.setColors(gridLineColor, gridLineColor);
        gridHelper.position.y = yPlaneAdjustment;
        this.gridHelper = gridHelper;
        scene.add(gridHelper);

        return scene;
    };

    STLRenderer.prototype._initLights = function() {
        var lightIntensity = 1;
        var light = new THREE.DirectionalLight(lightColor, lightIntensity);
        light.position = this.camera.position;

        this.scene.add(light);
        this.scene.add(new THREE.AmbientLight(ambientLightColor));

        return light;
    };

    STLRenderer.prototype._initControls = function() {
        var self = this;

        var controls = new THREE.OrbitControls(this.camera, this.$webGlElement[0]);
        controls.rotateSpeed = 1;
        controls.zoomSpeed = 0.5;
        controls.noRotate = false;
        controls.noZoom = false;
        controls.noPan = true;
        controls.noRoll = true;
        controls.dynamicDampingFactor = 0.5;

        // set up auto rotate when no interaction after a delay
        var timeoutId;

        function deferStartRotate() {
            timeoutId = window.setTimeout(function() {
                self.rotate = true;
            }, rotateDelay);
        }

        controls.addEventListener('end', deferStartRotate);
        controls.addEventListener('start', function() {
            self.rotate = false;
            if (timeoutId) {
                window.clearTimeout(timeoutId);
            }
        });

        deferStartRotate();

        return controls;
    };

    STLRenderer.prototype._onWindowResize = function() {
        this.height = this._calculateHeight();
        this.width = this.$container.innerWidth();
        this.$webGlElement.attr('width', this.width);

        this.camera.aspect = this._calculateAspectRatio();
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(this.width, this.height);
    };

    STLRenderer.prototype._onFileLoaded = function(geometry) {
        geometry.computeBoundingBox();

        var maxSize = geometry.boundingBox.max.clone().sub(geometry.boundingBox.min);
        var maxLength = maxSize.length();

        // Find out how much 'y' is off (or into!) the ground, then scale y by the length
        var groundHeight = geometry.boundingBox.min.z / maxLength;

        var modelMesh = new THREE.Mesh(geometry);

        modelMesh.position.y = -groundHeight + yPlaneAdjustment;
        modelMesh.rotation.set(-Math.PI / 2, 0, 0);
        modelMesh.scale.set(1 / maxLength, 1 / maxLength, 1 / maxLength);
        this.rootObject.add(modelMesh);
        this.modelMesh = modelMesh;

        this.setMeshMode(defaultMode);
    };

    STLRenderer.prototype._calculateAspectRatio = function() {
        return this.width / this.height;
    };

    STLRenderer.prototype._calculateHeight = function() {
        var windowHeightRatio = 0.6;
        return $(window).height() * windowHeightRatio;
    };


    /* --- API --- */

    STLRenderer.prototype.render = function() {
        requestAnimationFrame(this.render);
        if (this.rotate) {
            this.rootObject.rotation.y += rotateIncrement;
            this.gridHelper.rotation.y += rotateIncrement;
        }

        this.controls.update();
        this.renderer.render(this.scene, this.camera);
    };

    STLRenderer.prototype.load = function(stlUrl) {
        this.loader.load(stlUrl, _.bind(this._onFileLoaded, this));
    };

    STLRenderer.prototype.setMeshMode = function(mode) {
        this.modelMesh.material.dispose();

        switch (mode) {
            case STLRenderer.modes.SOLID :
                this.modelMesh.material = new THREE.MeshPhongMaterial(meshColors);
                break;
            case STLRenderer.modes.WIREFRAME :
                this.modelMesh.material = new THREE.MeshPhongMaterial($.extend({}, meshColors, {wireframe: true}));
                break;
            default :
                this.modelMesh.material = new THREE.MeshNormalMaterial();
        }
    };

    STLRenderer.prototype.destroy = function() {
        $(window).off('resize', this._onWindowResize);
        this.modelMesh.material.dispose();
        this.modelMesh.geometry.dispose();
        this.gridHelper.geometry.dispose();
        this.gridHelper.material.dispose();
        this.scene.remove(this.rootObject);
        this.scene.remove(this.gridHelper);
        this.controls.reset();
    };

    return STLRenderer;
});