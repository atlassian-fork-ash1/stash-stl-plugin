define('stl/stl-handler', [
    'jquery'
], function(
    $
) {
    "use strict";

    var stlViewResourceKey = 'com.atlassian.bitbucket.server.plugin.bitbucket-stl-plugin:stl-view';
    var webglEnabled = ( function () { try { var canvas = document.createElement( 'canvas' ); return !! window.WebGLRenderingContext && ( canvas.getContext( 'webgl' ) || canvas.getContext( 'experimental-webgl' ) ); } catch( e ) { return false; } } )();

    /**
     * Extract the extension from file name
     * @param {Object} filePath - the file path object
     * @returns {string} extension of file
     */
    function getFileExtension(filePath) {
        if (filePath.extension) {
            return filePath.extension;
        }
        var splitName = filePath.name.split('.');
        return splitName[splitName.length - 1];
    }

    /**
     * @returns Raw url for file
     */
    function getRawUrl(fileChange) {
        var projectKey = fileChange.repository.project.key;
        var repoSlug = fileChange.repository.slug;
        var path = fileChange.path.components.join('/');
        var revisionId = fileChange.commitRange.untilRevision.id;

        return AJS.contextPath() + AJS.format('/projects/{0}/repos/{1}/browse/{2}?at={3}&raw',
            projectKey, repoSlug, path, encodeURIComponent(revisionId));
    }

    // Register a file-handler for .stl files
    return function(options) {
        var fileChange = options.fileChange;

        // Check if .stl file
        var isStl = getFileExtension(fileChange.path) === 'stl';

        if (isStl && options.contentMode === 'diff') {
            // Not implemented, yet ;) TODO

        } else if (isStl && options.contentMode === 'source') {
            if (!webglEnabled) {
                console.log('webgl must be enabled to render .stl files in 3D')
            } else {
                // Asynchronously load stl-view web-resources (js/css/soy)
                var deferred = new $.Deferred();

                WRM.require('wr!' + stlViewResourceKey).done(function() {
                    // When web-resources successfully loaded, create a STLView
                    var STLView = require('stl/stl-view');
                    var fileUrl = getRawUrl(fileChange);
                    var view = new STLView(options.$container, fileUrl);

                    // This class gets added to the file-content
                    view.extraClasses = 'stl-file';

                    deferred.resolve(view);
                }).fail(function() {
                    /* Something went wrong trying to load the web-resources asynchronously,
                     and therefore we can't handle the file so reject the promise */
                    console.log('error while asynchronously loading stl-view resources');
                    return deferred.reject();
                });

                return deferred;
            }
        }

        // We can't handle this file
        return false;
    }
});


require('bitbucket/feature/files/file-handlers').register({
    weight: 900,
    handle: function(options) {
        return require('stl/stl-handler').apply(this, arguments);
    }
});

